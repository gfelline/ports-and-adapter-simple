package bootcamp.hexagonal.domain;

import bootcamp.hexagonal.port.BlogPort;
import bootcamp.hexagonal.port.PersistencePort;
import bootcamp.hexagonal.port.SocialMediaPort;
import bootcamp.hexagonal.servicelocator.ServiceLocator;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Collection;
import java.util.Optional;

public class Blog implements BlogPort {
    private final PersistencePort persistencePort;
    private final Collection<SocialMediaPort> socialMediaPorts;

    public Blog() {
        throw new NotImplementedException(); // TODO
    }

    public void post(String title, String text) {
        throw new NotImplementedException(); // TODO
    }

    public Optional<BlogPost> getPostWithTitle(String title) {
        throw new NotImplementedException(); // TODO
    }

    private Optional<SocialMediaPort> socialMediasWithName(String name) {
        return socialMediaPorts.stream().filter(socialMediaPort -> socialMediaPort.name().equals(name)).findFirst();
    }

    public void share(String title, String socialMediaName) {
        throw new NotImplementedException(); // TODO
    }

}
