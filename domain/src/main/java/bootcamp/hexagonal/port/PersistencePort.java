package bootcamp.hexagonal.port;

import bootcamp.hexagonal.domain.BlogPost;

import java.util.Optional;

public interface PersistencePort {
    void savePost(BlogPost blogPost);

    Optional<BlogPost> findPostWithTitle(String title);
}
