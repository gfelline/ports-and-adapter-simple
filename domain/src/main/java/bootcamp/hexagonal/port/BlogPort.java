package bootcamp.hexagonal.port;

import bootcamp.hexagonal.domain.BlogPost;

import java.util.Optional;

public interface BlogPort {

    void post(String title, String text);

    Optional<BlogPost> getPostWithTitle(String title);

    void share(String title, String socialMediaName);
}
