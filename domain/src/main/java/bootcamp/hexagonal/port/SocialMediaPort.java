package bootcamp.hexagonal.port;

import bootcamp.hexagonal.domain.BlogPost;

public interface SocialMediaPort {
    void share(BlogPost blogPost);
    String name();
}
