package bootcamp.hexagonal.facebook;

import bootcamp.hexagonal.domain.BlogPost;
import bootcamp.hexagonal.port.SocialMediaPort;

public class FacebookAdapter implements SocialMediaPort {

    @Override
    public void share(BlogPost blogPost) {
        System.out.println("Sharing " + blogPost.toString() + " on " + name());
    }

    @Override
    public String name() {
        return "Facebook";
    }
}
