package bootcamp.hexagonal.app;

import bootcamp.hexagonal.webserver.WebServerAdapter;

public class BlogApp {
    public static void main(String[] args) throws Exception {
        new WebServerAdapter().start();
    }
}
