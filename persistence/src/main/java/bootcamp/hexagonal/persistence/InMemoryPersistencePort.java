package bootcamp.hexagonal.persistence;

import bootcamp.hexagonal.domain.BlogPost;
import bootcamp.hexagonal.port.PersistencePort;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class InMemoryPersistencePort implements PersistencePort {

    private Map<String, String> posts = new HashMap<>();

    @Override
    public void savePost(BlogPost blogPost) {
        posts.put(blogPost.getTitle(), blogPost.getBody());
    }

    @Override
    public Optional<BlogPost> findPostWithTitle(String title) {
        return Optional.ofNullable(posts.getOrDefault(title, null))
                .map(body -> new BlogPost(title, body));
    }
}
