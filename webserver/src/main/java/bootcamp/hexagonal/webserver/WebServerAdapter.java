package bootcamp.hexagonal.webserver;

import bootcamp.hexagonal.port.BlogPort;
import bootcamp.hexagonal.servicelocator.ServiceLocator;
import ratpack.server.RatpackServer;

public class WebServerAdapter {

    public WebServerAdapter() {
        System.out.println("Webserver");
    }

    public void start() {
        BlogPort blogPort = ServiceLocator.lookup(BlogPort.class).orElseThrow(RuntimeException::new);
        try {
            RatpackServer.start(server -> server
                    .handlers(chain -> chain
                            .get("post/:title/:text", ctx -> {
                                String title = ctx.getPathTokens().get("title");
                                blogPort.post(title, ctx.getPathTokens().get("text"));
                                ctx.render("Posted blogPort " + blogPort.getPostWithTitle(title) + "!");
                            })
                            .get("article/:title", ctx -> {
                                ctx.render(blogPort.getPostWithTitle(ctx.getPathTokens().get("title"))
                                        .map(p -> "Blog post : " + p.toString() + "!")
                                        .orElse("Blog post not found !")
                                );
                            })
                            .get("share/:title/:media", ctx -> {
                                blogPort.share(ctx.getPathTokens().get("title"),ctx.getPathTokens().get("media"));
                                ctx.render("OK");
                            })
                    )
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
